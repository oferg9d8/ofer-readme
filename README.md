### What is this doc

A quick summary of how I operate. Yes it’s a weird text, and yet I believe it can be helpful. It should help introduce you to my management style, philosophy, and expectations. Who is it for? Anyone new on my teams, current team members and anyone interested.

This README is Inspired by [this](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe) article and examples.



### My job

**Attract and Retain world-class talent (that's you)**

 - Making sure you are happy, successful and enjoy what you’re doing.
   
-   Enable you to grow your skills.
    
-   Hiring is a big passion of mine. I try to build the right ensemble of people to have a mixture of talent that will serve the team best. I believe in including you in the process of hiring new team members (if you’d like to).
    

  

**Build well functioning and efficient teams**

-   Make sure the team operates efficiently.
    
-   You’ll be included in decision regarding the team. How the team operates, technical decision and so on.
    
-   I trust you and your peers to know what’s best for the team
    

  

**Set context**

-   Make sure you understand the bigger picture, why we do what we do, why we operate the way we do, how other teams are working with us and so on.
    
-   Drive longer term projects and initiative. Make sure we keep in mind things that are beyond the next sprint or two.
    
-   Advocating for the teams on other parts of the organization
    

**Additionally**: My job is not to tell you exactly what to do and how to do it.




### Relationships and communication

-   I’m usually at the office during regular office hours. If you need to get a hold of me, Slack is your best bet. If that fails, call me.
    
-   I’m usually available after hours and on weekends as well. I do not expect you to be available after hours. If I send you something over the weekend, it’s perfectly fine to ignore it till Monday.
    

-   We’ll set time to meet 1x1 on a regular basis. This meeting is not for project update. We’ll cover many [other topics](https://twitter.com/b0rk/status/1037186572234498048) and get to know each other better. If one of us is working remotely on a certain week, I prefer we reschedule or skip if there is nothing burning.
    

-   You can’t over-communicate, with me or with your peers. Communicate and share often and on all means.
    
-   Disagree with me and don’t hesitate to give me feedback.
    




### Management style and some philosophies I believe in

-   I assume you are able to operate as a professional adult. I trust you, your judgment, and your skills.
    
-   You are part of a team and never expected to figure out everything on your own. Ask for help often, and serve help as often.
    
-   I also trust your judgment on things like work life balance, your working hours and how you work with your peers. Lucky for us, it’s also a Points thing, that’s why there is no dress code, and if you need to take the morning from home for whatever reason - no one will question that.
    
-   Agile is an adjective, not a noun. Something needs to change? Let’s change it.
    
-   You should be much better than me at your job, if not, let’s make sure there is a clear path to get you there.
    
-   The way I see it, I’m measured in my absence as well. If I’m away for a few weeks, it shouldn’t be a big deal. Most of my job is focused on longer term, and the teams should be able to comfortably handle the day to day without me.
    
-   Kaizen. We should constantly improve. The first and second derivative are more important than where we are.
    



  

### Personal stuff

-   Married, three kids
    
-   Outdoor/hiking/canoeing, DIY, Artsy.
    
-   Diversity and inclusion are really dear to my heart, on and off the job


